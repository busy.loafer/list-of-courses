import { AxiosResponse } from "axios";
import { ICourse } from "./dataInterfaces";

export interface ICourseDataResponse extends AxiosResponse {
  data: ICourse[]
}