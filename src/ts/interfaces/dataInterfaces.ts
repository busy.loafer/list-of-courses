export interface ICourse {
  bgColor: string,
  id: string,
  image: string,
  name: string,
  tags: string[]
}

export type SortedDataType = {
  [key: string]: ICourse[]
}

export type SortedCoursesByTagsType = {
  title: string,
  data: ICourse[],
  id: string
}