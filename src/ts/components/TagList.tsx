import { FunctionComponent, useState } from "react"

interface ITagList {
  tags: string[],
  handleChangeTagIndex: (index: number) => void
}

const TagList: FunctionComponent<ITagList> = ({ tags, handleChangeTagIndex }) => {

  const [activeTagIndex, setActiveTagIndex] = useState<number>(0)

  const onSelectItem = (index: number) => {
    setActiveTagIndex(index)
    handleChangeTagIndex(index)
  }

  return <div className='tag_list'>
    {tags.map((tag: string, i: number) => {
      return <div
        className={`title tag_item${activeTagIndex === i ? "_active" : ""}`}
        onClick={() => onSelectItem(i)}
        key={i}
      >
        {tag}
      </div>
    })}
  </div>
}

export default TagList