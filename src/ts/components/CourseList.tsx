import { FunctionComponent } from "react"
import { ICourse } from "../interfaces/dataInterfaces"


const CourseList: FunctionComponent<{ courses: ICourse[] }> = ({ courses }) => {
  return <div className='course_list'>
    {courses.map((course: ICourse) => {
      const { bgColor, id, image, name } = course
      return <div className="course_item" key={id}>
        <div
          className="course_item_cover"
          style={{
            backgroundColor: bgColor
          }}
        >
          <img src={image} alt={name} />
        </div>
        <div className="course_item_title">{name}</div>
      </div>
    })}
  </div>
}

export default CourseList