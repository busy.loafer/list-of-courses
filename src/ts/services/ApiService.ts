import axios from "axios";
import { ICourseDataResponse } from "../interfaces/apiInterfaces";
import { ICourse } from "../interfaces/dataInterfaces";

const API_PATHS = {
  courses: "docs/courses.json",
};

const getCourses = async (): Promise<ICourse[]> => {
  const url = process.env.REACT_APP_API_URL + API_PATHS.courses;
  const res: ICourseDataResponse = await axios.get(url)
  if (res.data) {
    return res.data
  }
  return []
}

export { getCourses };