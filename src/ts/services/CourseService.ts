import { ICourse, SortedCoursesByTagsType, SortedDataType } from "../interfaces/dataInterfaces"
import { getCourses } from "./ApiService"

const sortCourses = (coursesData: ICourse[]): SortedCoursesByTagsType[] => {
  const sortData: SortedDataType = {}
  const coursesByTags: SortedCoursesByTagsType[] = [
    {
      title: "Все темы",
      data: [...coursesData],
      id: "tag:all"
    }
  ]
  coursesData.forEach((course: ICourse, cI: number) => {
    course.tags.forEach((title: string, index: number) => {
      if (sortData[title]) {
        sortData[title].push(course)
      } else {
        sortData[title] = [course]
      }
    })
  })
  for (let key in sortData) {
    let value = sortData[key];
    coursesByTags.push({
      title: key,
      data: value,
      id: `tag:${coursesByTags.length}`
    })
  }

  return coursesByTags
}

const getSortedCourses = async (): Promise<SortedCoursesByTagsType[]> => {
  const data: ICourse[] = await getCourses()
  return sortCourses(data)
}

export {getSortedCourses}