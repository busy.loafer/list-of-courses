import { FunctionComponent, useEffect, useRef, useState } from 'react';
import { ICourse, SortedCoursesByTagsType } from './interfaces/dataInterfaces';
import { getSortedCourses } from './services/CourseService';
import CourseList from './components/CourseList';
import TagList from './components/TagList';


const App: FunctionComponent = () => {

  const coursesByTags = useRef<Array<SortedCoursesByTagsType>>([])

  useEffect(() => {
    getCourses()
  }, [])

  const getCourses = async () => {
    coursesByTags.current = await getSortedCourses()
    setCourses(coursesByTags.current[0]?.data || [])
    setTags(coursesByTags.current.map((data: SortedCoursesByTagsType) => data.title))
  }

  const [courses, setCourses] = useState<ICourse[]>([])
  const [tags, setTags] = useState<string[]>([])

  const handleChangeTagIndex = (index: number) => {
    setCourses(coursesByTags.current[index]?.data || [])
  }


  return (
    <div className="courses_wrapper">
      <TagList tags={tags} handleChangeTagIndex={handleChangeTagIndex} />
      <CourseList courses={courses} />
    </div>
  );
}

export default App;
